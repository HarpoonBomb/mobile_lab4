package com.example.olejacob.mobdev_lab4;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagesFragment extends android.support.v4.app.Fragment {

    FirebaseFirestore db;
    RecyclerView messageList;
    String uid;
    String nickname;

    public MessagesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences userPreferences = getActivity().getSharedPreferences("userPreferences", Context.MODE_PRIVATE);
        uid = userPreferences.getString("uid", "");
        nickname = userPreferences.getString("nickname", "");
    }

    void UpdateMessages() {
        db.collection("messages").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<MessagesFragment.MessageItem> messages = new ArrayList<>();
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        MessagesFragment.MessageItem item = new MessageItem(document.getDate("date"),
                                document.getString("message"),
                                document.getString("user"),
                                document.getString("uid"));
                        messages.add(item);
                    }
                    Collections.sort(messages, new Comparator<MessagesFragment.MessageItem>() { //Sort by time
                        @Override
                        public int compare(MessagesFragment.MessageItem item1, MessagesFragment.MessageItem item2) {
                            return item1.date.compareTo(item2.date);
                        }
                    });
                    final List<MessagesFragment.MessageItem> finalMessages = messages;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            messageList.setAdapter(new MessageListAdapter(finalMessages));
                        }
                    });
                } else {
                    Log.d("DBGet", "Error getting messages");
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        db = FirebaseFirestore.getInstance();
        new Thread(new Runnable() {
            @Override
            public void run() {
                UpdateMessages();
            }
        }).start();
        View messageView = inflater.inflate(R.layout.fragment_messages, container, false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        messageList = messageView.findViewById(R.id.messageList);
        messageList.setLayoutManager(layoutManager);
        messageList.setAdapter(new MessageListAdapter(null));
        messageList.addItemDecoration(new DividerItemDecoration(messageList.getContext(), layoutManager.getOrientation()));
        Button sendButton = messageView.findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText messageField = getActivity().findViewById(R.id.messageField);
                String messageText = messageField.getText().toString();
                if (!messageText.isEmpty()) {
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    messageField.setText("");
                    Map<String, Object> message = new HashMap<>();
                    message.put("date", FieldValue.serverTimestamp());
                    message.put("message", messageText);
                    message.put("user", nickname);
                    message.put("uid", uid);
                    db.collection("messages").add(message).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            UpdateMessages();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("DbAdd", "Error adding message: " + e);
                        }
                    });
                }
            }
        });
        db.collection("messages").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                if (e != null) {
                    Log.e("UpdateListener", "Listen failed, " + e);
                } else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(5000);
                                UpdateMessages();
                            } catch (InterruptedException error) {
                                Log.e("InterruptedException", "Error: " + error);
                            }
                        }
                    }).start();
                }
            }
        });
        return messageView;
    }

    public class MessageItem {
        private Date date;
        private String message;
        public String user;
        public String uid;

        private MessageItem(Date date, String message, String user, String uid) {
            this.date = date;
            this.message = message;
            this.user = user;
            this.uid = uid;
        }
    }

    public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.MessageListViewHolder> {
        private List<MessageItem> messageItems;

        public class MessageListViewHolder extends RecyclerView.ViewHolder {
            private View messageListView;

            public MessageListViewHolder(View view) {
                super(view);
                messageListView = view;
            }
        }

        private MessageListAdapter(List<MessageItem> _messageItems) { messageItems = _messageItems; }

        @Override
        public MessageListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_list_layout, parent, false);
            return new MessageListViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MessageListViewHolder holder, int position) {
            final MessageItem messageItem = messageItems.get(position);
            ((TextView)holder.messageListView.findViewById(R.id.message))
                    .setText(messageItem.user + " (" + messageItem.date.toString() +"): " + messageItem.message);
        }

        @Override
        public int getItemCount() {
            if (messageItems != null) {
                return messageItems.size();
            } else {
                return 0;
            }
        }
    }
}

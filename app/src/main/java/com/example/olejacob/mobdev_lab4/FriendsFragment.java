package com.example.olejacob.mobdev_lab4;

import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FriendsFragment extends android.support.v4.app.Fragment {

    RecyclerView friendList;
    FirebaseFirestore db;

    public FriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        db = FirebaseFirestore.getInstance();
        db.collection("users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<FriendItem> friends = new ArrayList<>();
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Log.d("DOCUMENT", document.getString("nickname"));
                        FriendItem item = new FriendItem(document.getString("uid"), document.getString("nickname"));
                        friends.add(item);
                    }
                    Collections.sort(friends, new Comparator<FriendItem>() { //Alphabetical sort
                        @Override
                        public int compare(FriendItem item1, FriendItem item2) {
                            return item2.nickname.compareTo(item1.nickname);
                        }
                    });
                    final List<FriendItem> finalFriends = friends;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            friendList.setAdapter(new FriendListAdapter(finalFriends));
                        }
                    });
                } else {
                    Log.d("DBGet", "Error getting users");
                }
            }
        });
        View friendView = inflater.inflate(R.layout.fragment_friends, container, false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        friendList = friendView.findViewById(R.id.friendList);
        friendList.setLayoutManager(layoutManager);
        friendList.setAdapter(new FriendListAdapter(null));
        friendList.addItemDecoration(new DividerItemDecoration(friendList.getContext(), layoutManager.getOrientation()));
        return friendView;
    }

    public class FriendItem {
        private String uid;
        private String nickname;

        private FriendItem(String uid, String nickname) {
            this.uid = uid;
            this.nickname = nickname;
        }
    }

    public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.FriendListViewHolder> {
        private List<FriendItem> friendItems;

        public class FriendListViewHolder extends RecyclerView.ViewHolder {
            private View friendListView;

            public FriendListViewHolder(View view) {
                super(view);
                friendListView = view;
            }
        }

        private FriendListAdapter(List<FriendItem> _friendItems) { friendItems = _friendItems; }

        @Override
        public FriendListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_list_layout, parent, false);
            return new FriendListViewHolder(view);
        }

        @Override
        public void onBindViewHolder(FriendListViewHolder holder, int position) {
            final FriendItem friendItem = friendItems.get(position);
            ((TextView)holder.friendListView.findViewById(R.id.uid)).setText(friendItem.uid);
            ((TextView)holder.friendListView.findViewById(R.id.nickname)).setText(friendItem.nickname);
            /*holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Do stuff when name clicked
                }
            });*/
        }

        @Override
        public int getItemCount() {
            if (friendItems != null) {
                return friendItems.size();
            } else {
                return 0;
            }
        }
    }
}

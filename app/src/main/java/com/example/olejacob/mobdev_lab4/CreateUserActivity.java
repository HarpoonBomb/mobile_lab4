package com.example.olejacob.mobdev_lab4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class CreateUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        Button button = findViewById(R.id.nicknameButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editText = findViewById(R.id.namefield);
                String nickname = editText.getText().toString();
                if (nickname.isEmpty()) {
                    setResult(RESULT_CANCELED);
                    finish();
                } else {
                    CreateUser(nickname);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        EditText editText = findViewById(R.id.namefield);
        String nickname = editText.getText().toString();
        if (nickname.isEmpty()) {
            setResult(RESULT_CANCELED);
            finish();
        } else {
            CreateUser(nickname);
        }
    }

    public void CreateUser(final String nickname) {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        final SharedPreferences userPreferences = this.getSharedPreferences("userPreferences", MODE_PRIVATE);

        final FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    final FirebaseUser firebaseUser = auth.getCurrentUser();
                    Map<String, Object> user = new HashMap<>();
                    user.put("uid", firebaseUser.getUid());
                    user.put("nickname", nickname);
                    user.put("type", "normal");
                    db.collection("users").add(user).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            SharedPreferences.Editor userPrererenceEditor = userPreferences.edit();
                            userPrererenceEditor.putString("uid", firebaseUser.getUid());
                            userPrererenceEditor.putString("nickname", nickname);
                            userPrererenceEditor.apply();
                            Intent intent = new Intent();
                            intent.putExtra("uid", firebaseUser.getUid());
                            intent.putExtra("nickname", nickname);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("DbAdd", "Error adding document: " + e);
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
                } else {
                    Log.d("Authentication", "Authentication failed");
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });
    }
}

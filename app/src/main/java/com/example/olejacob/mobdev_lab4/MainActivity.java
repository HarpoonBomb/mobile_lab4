package com.example.olejacob.mobdev_lab4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    String uid;
    public String nickname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Load preferences
        SharedPreferences userPreferences = this.getSharedPreferences("userPreferences", MODE_PRIVATE);
        uid = userPreferences.getString("uid", "");
        nickname = userPreferences.getString("nickname", "");

        if (uid == "") { //No user, create one
            startActivityForResult(new Intent(MainActivity.this, CreateUserActivity.class), 1);
        } else { //Set up tabs
            ViewPager viewPager = findViewById(R.id.container);
            setupViewPager(viewPager);
            TabLayout tabLayout = findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
            //tabLayout.getTabAt(0).setIcon(R.drawable.);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) { //Keep trying until a user is created
                startActivityForResult(new Intent(MainActivity.this, CreateUserActivity.class), 1);
            } else if (resultCode == RESULT_OK) {
                SharedPreferences userPreferences = this.getSharedPreferences("userPreferences", MODE_PRIVATE);
                uid = userPreferences.getString("uid", "");
                nickname = userPreferences.getString("nickname", "");
            }
        }
    }

    void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MessagesFragment(), "MESSAGES");
        adapter.addFragment(new FriendsFragment(), "FRIENDS");
        viewPager.setAdapter(adapter);
    }
}

class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> fragmentList = new ArrayList<>();
    private final List<String> titleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }

    public void addFragment(Fragment fragment, String title) {
        fragmentList.add(fragment);
        titleList.add(title);
    }
}
